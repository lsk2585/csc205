1. Part 1 Introduction

2. A. Logic
    on/off switches represent true/false
        off switches do not pass an electrical current, assigned a numerical value of zero
        on switches pass electrical current, asigned numerical value of one

        electronic on-off switch is a singular line with two consecutive swtiches
    
3. B. Electronic Logic
    three basic logic statemnts
        and or not
        can combine statements such as not and (0 and 1 -> 1), or not or (0 or 0 -> 1)c

    three or more logic circuits make a logic system
        most basic is exclusive-or circuit
            used for impementing logical functions, or can be used to add two input conditions
            called a binary adder, as logical circuits are compatiable with the binary system

        two NAND circuits can form a flip flop circuit 
            bistable 
                changes state when incoming signal in pulse arrives, acts as short term memory element
                can be linked together to form elctronic counter and memory register

        logic circuits can form monostable and astable circuits
            monostable 
                occupy one of two states unless an incoming pulse is received then occupy occupy state for breif time, and resume normal state
            astable
                contiunally swtich back and forth between states
        

4. C. Number Systems
    
    digits of binary are bits

    eight and sixteen based number systems are compatible with logic systems becuase they provide shorthand for long binary

5. D. The Binary System

    ALTAIR 8800 performs nearly all operations in binary
        typically processes 8 bits e.g. 10111010
        fixed length binary number called a word or byte
            computers designed to process and store a fixed number of words
        counting in a number system rule: record successive digits for each count in a coulumn, when total number of available digits have been used, begin new coulumn to left of first and resume counting