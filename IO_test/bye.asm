	INCLUDE	stdlib	; Include standard I/O library

	ORG	000h	; Load at memory location 000 (hex)

	CALL RSTIO	; Initialize serial input / output bus
	LXI	B, WORDS	; Point to WORDS
	CALL  WRITE	; Write WORDS to stdout (terminal)

HCF:	HLT		; Halt and Catch Fire ;-)

; Data segment

	ORG	0200h	; Load at memory locaton 200 (hex)

WORDS:	DB	"Good-bye, cruel world!"
	DB	CR,LF	; Old-school CRLF newline
	DB	NUL	; NULL string terminator

	END