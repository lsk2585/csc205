
	TITLE	"IO_Pain - Copyrigth (C) "
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; NAME:    IO_Pain
; EDITOR:  
; LASTMOD: 
;
; DESCRIPTION:
;
;    This program takes a character stores it and outputs it
;	 on the screen
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;
; Constants ;
;;;;;;;;;;;;;

; ASCII characters

CR:		EQU	0Dh	; ASCII CR  (Carriage Return, a.k.a. Ctrl-M)
LF:		EQU	0Ah	; ASCII LF  (Line Feed        a.k.a. Ctrl-J)
ESC:	EQU	1Bh	; ASCII ESC (Escape,          a.k.a. Ctrl-[)
NUL:	EQU	00h	; ASCII NUL (Null)

; I/O

SIO1S:	EQU	10h	; Serial I/O communications port 1 STATUS
SIO1D:	EQU	11h	; Serial I/O communications port 1 DATA

MRST:	EQU	03h	; UART Master Reset
RCVD:	EQU	01h	; Character received
SENT:	EQU	02h	; Data sent. Output complete

; Code segment

	ORG	000h	; Load at memory location 0000 (hex)

	CALL RST

;Displays Question
; Get a character off of the serial I/O bus (stdin)
;Display question
    LXI	B,WORDS	; Point to WORDS
	PUSH PSW	; Preserve Program Status Word

FETCH:	IN	SIO1S	; Check serial I/O status bit 1 (XMIT status)
	ANI	SENT	; If data not sent (i.e. XMIT not finished)...
	JZ	FETCH	; ...spin wheels: continue checking status. Else...
	LDAX	B	; ...Fetch byte
	CPI	NUL	; ...If byte is ASCII NUL...
	JZ	RESP	; ......finish. Else...
	OUT	SIO1D	; ......output byte
	INX	B	; ......point to next byte
	JMP	FETCH	; ......lather, rinse, repeat: Fetch next byte.
    
	LXI	B,CHAR	; Point to buffer
	PUSH PSW	; Preserve Program Status Word


RESP: CALL RST

;Answers question
; Get a character off of the serial I/O bus (stdin)
;Grab typed charcter
	LXI	B,CHAR	; Point to buffer
	PUSH PSW	; Preserve Program Status Word

GETCHAR:  IN SIO1S ; Check serial I/O status
	ANI RCVD	; If data no data received
	JZ 	GETCHAR ; continue checking
	POP PSW		; restore Program Status Word
	IN	SIO1D	; read the character
	STAX B		; store it in buffer


; Put a character on to the serial I/O bus (stdout)
; Display typed charcter
	LXI B,CHAR	; Point to stored character
	LDAX B		; Fetch Byte
	PUSH PSW	; Preserve Program Status Word

OUTCHAR:  IN SIO1S ; Check serial I/O status bit 1 (XMIT status)
	ANI	SENT	; If data not sent (i.e. XMIT not finished)...
	JZ	OUTCHAR	; continue checking
	POP	PSW		; restore Program Status Word
	OUT	SIO1D	; output byte

;evaluate response and display result
	MVI A, 00
	ANI 00
	XRA A
	SUB A
	LDA 228h
	CPI 'a'; compare response to correct answer
	JZ ANS1 
 	LXI B, RES2
	 

ANS1: LXI B, RES1
	CALL RST ; display if correct or incorrect
	CALL DSPC ; display result


STOP: HLT







; Data Segment

	ORG	200h	; Load at memory location 200 (hex) 1000 (octal)

CHAR:	DB   000h	; Initialize to zero

; Reset serial input / output
;
RST: MVI	A,MRST
	OUT	SIO1S	; Reset the UART
	MVI	A,15h	; Settings: No RI, No XI, RTS Low, 8N1, /16
	OUT	SIO1S	; Configure the UART with above settings
    RET

DSPC:
	FETCH1:	IN	SIO1S	; Check serial I/O status bit 1 (XMIT status)
	ANI	SENT	; If data not sent (i.e. XMIT not finished)...
	JZ	FETCH1	; ...spin wheels: continue checking status. Else...
	LDAX	B	; ...Fetch byte
	CPI	NUL	; ...If byte is ASCII NUL...
	JZ	STOP	; ......finish. Else...
	OUT	SIO1D	; ......output byte
	INX	B	; ......point to next byte
	JMP	FETCH1	; ......lather, rinse, repeat: Fetch next byte.



WORDS:	DB	'Select a letter'
	DB	CR,LF	; Old-school CRLF newline
	DB 'a, b, c, d'
	DB	CR,LF	; Old-school CRLF newline
	DB	NUL	; NULL string terminator

COR: DB 'a'

RES1: DB CR,LF
      DB 'Correct!'
	  DB NUL

RES2: DB CR,LF
	  DB 'Incorrect!'
	  DB NUL 








