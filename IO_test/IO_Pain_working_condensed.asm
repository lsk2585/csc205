	INCLUDE	stdlib	; Include standard I/O library

; Code segment

	ORG	000h	; Load at memory location 0000 (hex)

	CALL RSTIO  ; Reset input/output

; Display Question

    LXI	B,WORDS	; Point to question string
	CALL WRITE  ; Print question string

; Collect Reponse

	CALL RSTIO ; Reset input/output

	; Grab typed charcter

	LXI	B,CHARC	; Point to buffer
	PUSH PSW	; Preserve Program Status Word

	CALL GETC ; Grab Charcter
	STAX B ; Store charcter in accumlator

; Evaluate response and display result
COMP:	LDA CHARC ; Load accumlator with response
    CPI 'a'
	;MVI D, 'a' ; Load register D with correct asnswer ; Enter correct answer here
    ;SUB D ; Compare correct answer to response with subtraction
    JZ ANS1 ; Answer is correct
    LXI B, RES2 ; Answer is incorrect, point to incorrect reponse
    CALL RSTIO ; Display if correct or incorrect
	CALL WRITE ; Display result
    JMP STOP ; Stop program

ANS1: LXI B, RES1 ; Point to correct response
	CALL RSTIO ; Reset I/O
	CALL WRITE ; Display evaluation of response

STOP: HLT ; Stop program

; Data Segment

	ORG	200h	; Load at memory location 200 (hex) 1000 (octal)

CHARC:	DB   000h	; Initialize to zero

; Questions and possible answers

WORDS:  DB	CR,LF	; Old-school CRLF newline
	DB	'In the 80s Jeff spent two years building houses. In what location did he do this?' ; Enter question here
	DB	CR,LF	; Old-school CRLF newline
	DB	CR,LF	; Old-school CRLF newline
	DB 'a) Nicaragua ' ; Answer choice a ; Enter multiple choice options here
	DB	CR,LF	; Old-school CRLF newline
	DB 'b) Willy Wonkas Factory **for the Oompaloompas** ' ; Answer choice B
	DB	CR,LF	; Old-school CRLF newline
	DB 'c) Outerspace ' ; Answer choice C
	DB	CR,LF	; Old-school CRLF newline
	DB 'd) Mars **outerspace, but in the future with Elon Musk** ' ; Answer choice D
	DB CR,LF	; Old-school CRLF newline
	DB CR,LF	; Old-school CRLF newline
	DB	NUL	; NULL string terminator

; Correct response

RES1: DB CR,LF	; Old-school CRLF newline
	  DB CR,LF ; Old-school CRLF newline
      DB 'Correct!' ; Enter correct answer reponse here
	  DB NUL ; NULL string terminator

; Incorrect response

RES2: DB CR,LF	; Old-school CRLF newline
	  DB CR,LF ; Old-school CRLF newline
	  DB 'Incorrect!' ; Enter incorrect answer response here
	  DB NUL ; NULL string terminator
