DIVID:        ORG 377o ; Set program counter tp 

        MVI	B,00h
	MVI	D,00h
	MVI	E,2	; divisor  (DE) =  9 decimal

DIVI:	MOV	A,D	; Negate the divisor
	CMA		;    "    "     "
	MOV	D,A	;    "    "     "
	MOV	A,E	;    "    "     "
	CMA		;    "    "     "
	MOV	E,A	;    "    "     "
	INX	D	; For two's complement
	LXI	H,00h	; Initial value for remainder
	MVI	A,11h	; Initialize loop counter
DV0:	PUSH	H	; Save remainder
	DAD	D	; Subtract divisor (add negative)
	JNC	DV1	; Underflow. Restore register pair HL
	XTHL
DV1:	POP	H
	PUSH	PSW	; Save loop counter
	MOV	A,C
	RAL		; 4 register left shift?
	MOV	C,A	; CY->C->B->L->H?
	MOV	A,B
	RAL
	MOV	B,A
	MOV	A,L
	RAL
	MOV	L,A
	MOV	A,H
	RAL
	MOV	H,A
	POP	PSW
	DCR	A
	JNZ	DV0

; Post-divide clean-up:
; Shift remainder right and return in DE

	ORA	A
	MOV	A,H
	RAR
	MOV	D,A
	MOV	A,L
	RAR
	MOV	E,A
	HLT

ADD:	ORG	62o	; Set Program Counter to address 50d
	LDA	VAL1	; Load value (5) at VAL1 (200) into Accumulator
	MOV	B,A	; Move value in Accumulator to Register B
	LDA	VAL2	; Load value (8) at VAL2 (201) into Accumulator
	ADD	B	; Add value in Register B to value in Accumulator
	STA	002	; Store accumulator at Register Pair B and C
             
        RET
; Data segment:

	ORG	200o	; Set Program Counter to address 200
VAL1:	DB	12o	; Data Byte at address 200 = 5
VAL2:	DB	12o	; Data Byte at address 201 = 10 (10 octal)

        
        
        ORG 0000h ; Set Program couner to 0
START:  CALL ADD
        CALL DIVID
        END
        
        




