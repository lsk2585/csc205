       ORG 000h
START: MVI	A,00Fh ; specific heat value
       MVI	D,00Ah	; grams of substance
       CALL MULT
       MVI  D,002h         ; change in temperature
       XCHG
       CALL MULT
	   SHLD	0054
       HLT		; Halt and Catch Fire (HCF)


	   ORG	0060h	; Set Program Counter to address 0
MULT:		; Multiplier    (-1) to A   Register
	MVI	E,000h
	LXI	H,000h	; Clear H,L Registers to initialize Partial Product
	MVI	B,008h	; Iteration Count (8) to B Register

LOOP:	DAD	H	; Shift Partial Product left into Carry (H&L)
	RAL		; Rotate Multiplier Bit to Carry
	JNC	NEXT	; Test Multiplier at Carry
	DAD	D	; Add Multiplicand to Partial Product (D&E)
			;   if Carry =1
	ACI	000h	; (Add Carry Bit)
NEXT:	DCR	B	; Decrement Iteration Counter
	JNZ	LOOP	; Check Iterations
;	JMP	START	; Restart
	RET
    



	ORG	040h	; Set Program Counter to address 100 (octal)
TOTAL:	DS	002h	; Reserve 2 bytes (1 word) of unintialized storage

	END		; End










